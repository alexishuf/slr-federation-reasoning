<html>
<head>
<meta charset="utf-8">
<style>
body {
  background-color: #161616;
  font-family: serif;
  color: #e8e8e8;
}
p {
  margin-bottom: 30px;
}
a {
    color: #879bf2;
}
.name {
  font-weight: bold;
}
.RDF {
  background-color: #5b411c;
}
.qry {
  background-color: #749e35;
}
.federation {
  background-color: #1c505b;
}
.OBDA {
  background-color: #1c5b39;
}
.multi {
  border: 1px solid #7a72fb;
}
.remote {
  background-color: #2f2a82;
}
.reason {
  background-color: #752b8c;
} 
.goal {
  border: 1px solid #ef4d4d;
}
.header {
     position: absolute;
     top:0px;
     right:0px;
     z-index:20;
}
</style>
</head>

<body>
<div class="header">
  <table>
  <tr>
  <td style="background-color: #5b411c;">RDF</td>
  <td style="background-color: #749e35;">query</td>
  <td style="background-color: #1c505b;">federation</td>
  <td style="background-color: #1c5b39;">OBDA</td>
  <td style="background-color: #7a72fb;">multi-database</td>
  <td style="background-color: #2f2a82;">remote</td>
  <td style="background-color: #752b8c;">reason</td>
  </tr>
  </table>
</div>
