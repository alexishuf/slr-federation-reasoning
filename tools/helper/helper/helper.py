import sys
import stats
import ids
import selection
import dedup
import extended


if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] == 'help' :
        print(f'Usage: python {sys.argv[0]} task [args...]\n')
        print(f'Tasks:')
        print(f'  stats input.bib [output]')
        print(f'  setup_ids old_ids.csv input.bib')
        print(f'  setup_selection ids.csv abstracts.html query.kws abstracts.csv')
        print(f'  exclude_loose input.csv tags.csv output.csv')
        print(f'  setup_dedup ids.csv tags.csv abstracts.csv dedup.html dedup.csv')
        print(f'  dedup abstracts.csv dedup.csv abstracts-dedup.csv')
        print(f'  setup_extended ids.csv tags.csv abstracts.csv extended.html extended.csv')
        print(f'  exclude_extended abstracts.csv extended.csv abstracts-extended.csv')
        print(f'  html ids.csv selection.csv out.html [query.kws [rel_pdfs_dir]] ')
        print(f'  help')
        exit(1)
    if sys.argv[1] == 'stats':
        stats.parse(sys.argv[2:])
    elif sys.argv[1] == 'setup_ids':
        ids.setup_ids_parse(sys.argv[2:])
    elif sys.argv[1] == 'setup_selection':
        selection.setup_selection_parse(sys.argv[2:])
    elif sys.argv[1] == 'exclude_loose':
        selection.exclude_loose_parse(sys.argv[2:])
    elif sys.argv[1] == 'setup_dedup':
        dedup.setup_parse(sys.argv[2:])
    elif sys.argv[1] == 'dedup':
        dedup.dedup_parse(sys.argv[2:])
    elif sys.argv[1] == 'setup_extended':
        extended.setup_parse(sys.argv[2:])
    elif sys.argv[1] == 'exclude_extended':
        extended.exclude_extended_parse(sys.argv[2:])
    elif sys.argv[1] == 'html':
        ids.write_html_parse(sys.argv[2:])

    
