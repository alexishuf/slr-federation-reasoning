import sys
import bibtexparser 
import numpy
import nltk
import csv
import shutil
import os
import re
import base64
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from pkg_resources import resource_string

def merge_id_entry(old_entries, e):
    merge = dict(e)
    old_e = None
    clear = lambda d, k: d[k].lower().strip() if k in d else ''
    la, lt = clear(e, 'author'), clear(e, 'title')
    if 'doi' in e:
        l = [x for x in old_entries if x['doi'] == e['doi']][:1]
        if (len(l)) > 0:
            old_e, merge['id'] = l[0], l[0]['id']
    if 'id' not in merge and 'author' in e and 'title' in e:
        l = [x for x in old_entries if clear(x, 'author') == la and clear(x, 'title') == lt]
        if len(l) > 0:
            old_e, merge['id'] = l[0], l[0]['id']
    if 'id' not in merge:
        labs = clear(e, 'abstract')
        l = [x for x in old_entries if clear(x, 'author') == la and clear(x, 'title') == lt \
             and clear(x, 'abstract') == labs]
        if len(l) > 0:
            old_e, merge['id'] = l[0], l[0]['id']
    author = e['author'] if 'author' in e else ''
    title = e['title'] if 'title' in e else ''
    abstract = e['abstract'] if 'abstract' in e else ''
    old_author = old_e['author'] if old_e != None else ''
    old_title = old_e['title'] if old_e != None else ''
    old_abstract = old_e['abstract'] if old_e != None and 'abstract' in old_e else ''
    old_kws = old_e['kws'] if old_e != None else ''
    if old_e != None:
        merge['id'] = old_e['id']
    merge['doi'] = e['doi'] if 'doi' in e else old_e['doi'] if old_e != None else ''
    merge['author'] = author if len(author) >= len(old_author) else old_author
    merge['title'] = title if len(title) >= len(old_title) else old_title
    merge['abstract'] = abstract if len(abstract) >= len(old_abstract) else old_abstract
    merge['kws'] = e['kws'] if len(e['kws']) >= len(old_kws) else old_kws
    return merge
    
def setup_ids(ids_csv, input_bib):
    old = []
    try:
        with open(ids_csv, newline='') as ids_file:
            old = [r for r in csv.DictReader(ids_file)]
    except FileNotFoundError:
        pass
        
    new = []
    id_base = str(base64.b64encode(os.urandom(3)), 'utf-8')
    next_id = max([0] + [int(x['id'][4:]) for x in old]) + 1
    print(f'id_base: {id_base}, next_id: {next_id}')
    
    additions, written = 0, 0
    missing_author, missing_title, missing_author_title = 0, 0, 0
    with open(input_bib) as bib_file, open(ids_csv, 'w', newline='') as out_csv:
        fieldnames=['id', 'doi', 'author', 'title', 'abstract', 'kws']
        writer = csv.DictWriter(out_csv, fieldnames=fieldnames)
        writer.writeheader()
        # rx = re.compile(r'^(\s*)author_(keywords)\s*=\s*')
        # bib_string = '\n'.join([rx.sub(r'keyword = ', line) for line in bib_file])
        db = bibtexparser.load(bib_file)
        for e in db.entries:
            kws = ' '.join([e[k] for k in e if 'keyword' in k])
            e = {k: e[k] for k in e if k in fieldnames}
            e['kws'] = kws
            row = merge_id_entry(old, e)
            if 'id' not in row:
                row['id'] = f'{id_base}-{next_id}'
                next_id, additions = next_id + 1, additions + 1
                if 'author' not in e and 'title' not in e:
                    missing_author_title = missing_author_title + 1
                elif 'author' not in e:
                    missing_author = missing_author + 1
                elif 'title' not in e:
                    missing_title = missing_title + 1
            writer.writerow(row)
            written = written + 1

    removed = len(old) - (written-additions)
    print(f'Added {additions} records to {ids_csv}, removed {removed}.')
    print(f'{missing_author} missing author, {missing_title} missing title,',
          f' {missing_author_title} missing both.')

            
def setup_ids_parse(argv):
    if len(argv) < 2:
        raise Exception('setup_ids: Too few arguments')
    setup_ids(argv[0], argv[1])

def load_hl_map(kws_file):
    # map CSS class name => [Pattern]
    hl_map = {};
    with open(kws_file, 'r') as hl_file:
        res = None
        for line in [x.strip() for x in hl_file]:
            if len(line) == 0:
                res = None
            elif res == None:
                res = hl_map[line] = []
            else:
                res.append(re.compile(f'({line})', re.IGNORECASE))
    return hl_map

def highlight_html(hl_map, text):
    if hl_map != None:
        for k in hl_map:
            for p in hl_map[k]:
                text = p.sub(f'<span class="{k}">\\1</span>', text)
    text = re.sub(r'((\Wpresents?\W|propose|goal|aim(s|ed|ing)?|demonstrate|show)[^.]*)',
                  r'<span class="goal">\1</span>', text)
    return text

def write_html(entries, selected, html_path, hl_map=None, pdf_dir=None):
    hl_names = ['Title', 'Abstract', 'Kws']
    with open(html_path, 'w') as html_file:
        html_file.write(str(resource_string('resources', 'abstracts.prolog'), 'utf-8'))
        for selection in selected:
            cands = [x for x in entries if x['id'] == selection['id']]
            assert len(cands) > 0, f'No candidates for {selection["id"]} in ids.csv file'
            entry = cands[0]
            doi, doi_url = entry['doi'], f'https://doi.org/{entry["doi"]}'
            authors = [a.split(',') for a in entry['author'].split(' and ')]
            authors = ' '.join([a[0] if len(a) > 1 else a[0].split(' ')[-1] for a in authors])
            scholar_qry = authors + ' ' + entry['title']
            html_file.write( '<p>\n')
            html_file.write(f'  <span class="name">ID</span>: {entry["id"]}&nbsp;&nbsp;')
            if pdf_dir != None:
                html_file.write(f'  <a href="{pdf_dir}/{entry["id"]}.pdf">PDF</a><br>\n')
            else:
                html_file.write(f'  <br>\n')
            tags_html = ' '.join([f'{t.upper()}' for t in selection['tags'].strip().split(' ')])
            html_file.write(f'  <span class="name">Tags</span>: {tags_html}<br>\n')
            html_file.write(f'  <span class="name">DOI</span>: <a href="{doi_url}">{doi}</a>\n')
            html_file.write(f'  <a href="https://scholar.google.com.br/scholar?q={scholar_qry}">Scholar</a><br>\n')
            html_file.write(f'  <span class="name">Author</span>: {entry["author"]}<br>\n')
            for name in hl_names:
                html_file.write(f'  <span class="name">{name}</span>: ' + \
                                f'{highlight_html(hl_map, entry[name.lower()])}<br>\n')
            html_file.write( '<p>\n\n')
        html_file.write(str(resource_string('resources', 'abstracts.epilog'), 'utf-8'))
    
def write_html_parse(argv):
    with open(argv[0], 'r', newline='') as entries_file, \
         open(argv[1], 'r', newline='') as selected_file:
        entries = [x for x in csv.DictReader(entries_file)]
        selection = [x for x in csv.DictReader(selected_file)]
        hl_map = None
        pdf_dir = None
        if len(argv) > 3:
            hl_map = load_hl_map(argv[3])
        if len(argv) > 4:
            pdf_dir = argv[4]
        write_html(entries, selection, argv[2], hl_map, pdf_dir)
