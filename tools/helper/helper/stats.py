import sys
import bibtexparser 
import numpy
import nltk
import csv
import shutil
import os
import re
import base64
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from pkg_resources import resource_string

def stats_write(array, out):
    out.write(f'mean: {numpy.mean(array)}\n')
    out.write(f'var: {numpy.var(array)}\n')
    out.write(f'max: {numpy.amax(array)}\n')
    for q in [50, 95, 99]:
        out.write(f'{q}% percentile: {numpy.percentile(array, q)}\n')

def compute(bib_path, out):
    nltk.download('punkt')
    print('Reading bibtex entries...')
    lens = []
    abs_lens = []
    missing = 0

    with open(bib_path, 'r') as bibtex_file:
        db = bibtexparser.load(bibtex_file)
        for e in db.entries:
            if 'abstract' not in e:
                missing = missing + 1
                continue
            print('.', end='', flush=True)
            abs_lens.append(len(word_tokenize(e['abstract'])))
            for sentence in sent_tokenize(e['abstract']):
                lens.append(len(word_tokenize(sentence)))
    print()
                
    out.write(f'{missing} entries out of {len(db.entries)} without the abstract!\n')
    out.write('Sentence length in words:\n')
    stats_write(lens, out)
    out.write('\nAbstract length in words:\n')
    stats_write(abs_lens, out)

def parse(argv):
    out = sys.stdout if len(sys.argv) < 2 else open(argv[1], 'w')
    try:
        compute(argv[0], out)
    finally:
        if out is not sys.stdout:
            out.close()
