import sys
import bibtexparser 
import numpy
import nltk
import csv
import shutil
import os
import re
import base64
import tags
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from pkg_resources import resource_string
from ids import highlight_html, load_hl_map

def maches_html(hl_map, text):
    html = ''
    for k in hl_map:
        if len([x for x in [p.search(text) for p in hl_map[k]] if x != None]):
            html = html + f' <span class="{k}">{k}</span>'
    return html
    
    
def setup_selection(hl_map, ids_csv, html, old_results):
    results = []
    html.write(str(resource_string('resources', 'abstracts.prolog'), 'utf-8'))
    hl_names = ['Title', 'Abstract', 'Kws']
    for row in csv.DictReader(ids_csv):
        doi, doi_url = row['doi'], f'https://doi.org/{row["doi"]}'
        matches = maches_html(hl_map, f'{row["title"]}. {row["abstract"]}. {row["kws"]}');
        html.write( '<p>\n')
        html.write(f'  <span class="name">ID</span>: {row["id"]}<br>\n')
        html.write(f'  <span class="name">Matches</span>: {matches}<br>\n')
        html.write(f'  <span class="name">DOI</span>: <a href="{doi_url}">{doi}</a><br>\n')
        html.write(f'  <span class="name">Author</span>: {row["author"]}<br>\n')
        for name in hl_names:
            html.write(f'  <span class="name">{name}</span>: ' + \
                       f'{highlight_html(hl_map, row[name.lower()])}<br>\n')
        html.write( '<p>\n\n')
        l = [x for x in old_results if x['id'] == row['id']]
        if len(l) > 0:
            results.append(l[0])
        else:
            results.append({'id': row['id'], 'tags': ''})
        html.write(str(resource_string('resources', 'abstracts.epilog'), 'utf-8'))
    return results

def setup_selection_parse(argv):
    if (len(argv) < 4):
        raise Exception(f'setup_selection: Insufficient arguments');
    # map CSS class name => [Pattern]
    hl_map = load_hl_map(argv[2]);
    # get old results
    results = []
    try:
        with open(argv[3], 'r', newline='') as results_file:
            results = [x for x in csv.DictReader(results_file)]
    except FileNotFoundError:
        pass
    # write updated html and results csv
    with open(argv[0], 'r', newline='') as ids_file, \
         open(argv[1], 'w', newline='') as out_file:
        results = setup_selection(hl_map, ids_file, out_file, results)
    # backup old results.csv
    has_bck = True
    try:
        shutil.copyfile(argv[3], argv[3] + '.bck');
    except FileNotFoundError:
        has_bck = False
    # write results
    with open(argv[3], 'w', newline='') as result_file:
        writer = csv.DictWriter(result_file, fieldnames=['id', 'tags'])
        writer.writeheader()
        for r in results:
            writer.writerow(r)
    if has_bck:
        os.remove(argv[3] + '.bck')


def exclude_loose(in_entries, tags, out_file):
    out = csv.DictWriter(out_file, fieldnames=['id', 'tags'])
    out.writeheader()
    for e in [x for x in in_entries if tags.is_inc_loose(x)]:
        out.writerow(e);

        
def exclude_loose_parse(argv):
    if len(argv) < 3:
        raise Exception('exclude: Insufficient arguments')
    noreason = '-noreason' in argv[3:]
    survey = '+survey' in argv[3:]
    in_entries = []
    tags_dict = tags.TagsDict(argv[1])
    for tag in [x[1:] for x in argv[3:] if x[:1] == '-']:
        tags_dict['exc'].remove(tag)
    for tag in [x[1:] for x in argv[3:] if x[:1] == '+']:
        tags_dict['exc'].add(tag)
    with open(argv[0], 'r', newline='') as in_file:
        in_entries = [x for x in csv.DictReader(in_file)]
    with open(argv[2], 'w', newline='') as out_file:
        exclude_loose(in_entries, tags_dict, out_file)
