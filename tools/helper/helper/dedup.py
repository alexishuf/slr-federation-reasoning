import csv
import tags
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from pkg_resources import resource_string

def surnames(authors):
    authors = [a.split(',') for a in authors.split(' and ')]
    return {a[0] if len(a) > 1 else a[0].split(' ')[-1] for a in authors}


def dedup_similar(a, b):
    # allow additional authors in one of the documents
    m = min(len(a['author']), len(b['author']))
    m = max(2, m) - 1
    if len(a['author'].intersection(b['author'])) < m:
        return False
    if a['title'] != b['title']:
        return False
    m = min(len(a['abstract']), len(b['abstract']))
    if m == 0:
        return True
    else:
        m = m - 1
    return a['abstract'][:m] == a['abstract'][:m]
    

def dedup_merge_eqset(canon, i, j):
    if canon[i] == canon[j]:
        return
    child = canon[j]
    for idx in range(0, len(canon)):
        if canon[idx] == child:
            canon[idx] = canon[i]

            
def write_document(html, csv_writer, entry):
    doi, doi_url = entry['doi'], f'https://doi.org/{entry["doi"]}'
    authors = [a.split(',') for a in entry['author'].split(' and ')]
    authors = ' '.join([a[0] if len(a) > 1 else a[0].split(' ')[-1] for a in authors])
    qry = authors + ' ' + entry['title']
    html.write( '<p>\n')
    html.write(f'  <span class="name">ID</span>: {entry["id"]}<br>\n')
    html.write(f'  <span class="name">DOI</span>: <a href="{doi_url}">{doi}</a>')
    html.write(f'  <a href="https://scholar.google.com.br/scholar?q={qry}">Scholar</a><br>\n')
    html.write(f'  <span class="name">Author</span>: {entry["author"]}<br>\n')
    html.write(f'  <span class="name">Title</span>: {entry["title"]}<br>\n')
    html.write(f'  <span class="name">Abstract</span>: {entry["abstract"]}<br>')
    html.write( '</p>\n')
    csv_writer.writerow({'id': entry['id'], 'decision': ''})


def setup(ids, tags_dict, tagged, html, csv_file):
    csv_writer = csv.DictWriter(csv_file, fieldnames=['id', 'decision'])
    inc_ids = {x['id'] for x in tagged if tags_dict.is_inc_loose(x)}
    inc = [dict(x) for x in ids if x['id'] in inc_ids]
    stopws = None
    try:
        stopws = set(stopwords.words('english'))
    except LookupError:
        nltk.download('stopwords')
    stopws = set(stopwords.words('english'))

    # Preprocess metadata
    for entry in inc:
        entry['author'] = surnames(entry['author'].lower())
        s = sent_tokenize(entry['title'].replace(':', '.').lower())[0]
        entry['title'] = [w for w in word_tokenize(s) if w not in stopws]
        entry['abstract'] = [[w for w in word_tokenize(s) if w not in stopws] \
                             for s in sent_tokenize(entry['abstract'].lower())]

    # Group similar documents
    canon = [x for x in range(0, len(inc))]
    for i in range(0, len(inc)):
        for j in range(i+1, len(inc)):
            if dedup_similar(inc[i], inc[j]):
                dedup_merge_eqset(canon, i, j)

    # Reverse canon into a list of members for each equivalence set
    members = [set() for x in range(0, len(inc))]
    for i in range(0, len(canon)):
        members[canon[i]].add(i)

    # Output each equivalence set
    html.write(str(resource_string('resources', 'abstracts.prolog'), 'utf-8'))
    csv_writer.writeheader()
    for group_idx in range(0, len(members)):
        if len(members[group_idx]) < 2:
            continue
        html.write(f'<h2>Group {group_idx}</h2>\n')
        for idx in members[group_idx]:
            entry = [x for x in ids if x['id'] == inc[idx]['id']][0]
            write_document(html, csv_writer, entry);
    html.write(str(resource_string('resources', 'abstracts.epilog'), 'utf-8'))


def setup_parse(argv):
    with open(argv[0], 'r', newline='') as ids_file, \
         open(argv[2], 'r', newline='') as abstracts_file, \
         open(argv[3], 'w', newline='') as html_file, \
         open(argv[4], 'w', newline='') as csv_file:
        ids = [x for x in csv.DictReader(ids_file)]
        tags_dict = tags.TagsDict(argv[1])
        tagged = [x for x in csv.DictReader(abstracts_file)]
        setup(ids, tags_dict, tagged, html_file, csv_file)

def dedup(rows, dedup_rows, csv_file):
    # remove duplicate ids
    rem = {i for i in range(len(rows)) if \
           len([j for j in range(i) if rows[j]['id'] == rows[i]['id']]) >0}
    rows = [rows[i] for i in range(len(rows)) if i not in rem]    
    # set dup and ext tags
    for e in dedup_rows:
        if len(e['decision'].strip()) > 0:
            candidates = [x for x in rows if x['id'] == e['id']]
            if len(candidates) > 0:
                candidates[0]['tags'] = candidates[0]['tags'] + ' ' + e['decision']
    csv_writer = csv.DictWriter(csv_file, fieldnames=['id', 'tags'])
    csv_writer.writeheader()
    for e in rows:
        csv_writer.writerow(e)


def dedup_parse(argv):
    with open(argv[0], 'r', newline='') as abstracts_file, \
         open(argv[1], 'r', newline='') as dedup_file, \
         open(argv[2], 'w', newline='') as csv_file:
        rows = [x for x in csv.DictReader(abstracts_file)]
        dedup_rows = [x for x in csv.DictReader(dedup_file)]
        dedup(rows, dedup_rows, csv_file)
    
        
