import csv
import tags
import nltk
import string
import collections
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import DBSCAN
from pkg_resources import resource_string

def write_document(html, csv_writer, entry):
    doi, doi_url = entry['doi'], f'https://doi.org/{entry["doi"]}'
    authors = [a.split(',') for a in entry['author'].split(' and ')]
    authors = ' '.join([a[0] if len(a) > 1 else a[0].split(' ')[-1] for a in authors])
    qry = authors + ' ' + entry['title']
    html.write( '<p>\n')
    html.write(f'  <span class="name">ID</span>: {entry["id"]}<br>\n')
    html.write(f'  <span class="name">DOI</span>: <a href="{doi_url}">{doi}</a>')
    html.write(f'  <a href="https://scholar.google.com.br/scholar?q={qry}">Scholar</a><br>\n')
    html.write(f'  <span class="name">Author</span>: {entry["author"]}<br>\n')
    html.write(f'  <span class="name">Title</span>: {entry["title"]}<br>\n')
    html.write(f'  <span class="name">Abstract</span>: {entry["abstract"]}<br>')
    html.write( '</p>\n')
    csv_writer.writerow({'id': entry['id'], 'decision': ''})

def preproc_text(text, stem=True):
    text = text.translate(str.maketrans('','',string.punctuation))
    tokens = word_tokenize(text)
    if stem:
        stemmer = PorterStemmer()
        tokens = [stemmer.stem(t) for t in tokens]
    return tokens    

def setup(ids, tags_dict, tagged, html, csv_file, eps=0.05):
    csv_writer = csv.DictWriter(csv_file, fieldnames=['id', 'decision'])
    inc_ids = {x['id'] for x in tagged if tags_dict.is_inc_loose(x)}
    inc = [{'id': x['id'], 'text': x['title'] + '.\n' + x['abstract']} \
           for x in ids if x['id'] in inc_ids]
    vectorizer = TfidfVectorizer(tokenizer=preproc_text,
                                 stop_words=stopwords.words('english'),
                                 max_df=0.5, # 50% of documents
                                 min_df=2,   #  2  document
                                 lowercase=True)
    tfidf_model = vectorizer.fit_transform([x['text'] for x in inc])
    db = DBSCAN(eps=eps, min_samples=2, metric='cosine', algorithm='brute')
    db_model = db.fit(tfidf_model)
    clusters = collections.defaultdict(list)
    for idx, label in enumerate(db_model.labels_):
        clusters[label].append(idx)

    sizes = {}
    for cluster_idx in [x for x in clusters if x >= 0]:
        s = len(clusters[cluster_idx])
        sizes[s] = (sizes[s] if s in sizes else 0) + 1

    print(f'Found {len(clusters) - (1 if -1 in clusters else 0)} clusters')
    print('Cluster size histogram:')
    for k, v in sorted(sizes.items(), key=lambda kv: kv[0]):
        print(f'{k:3} {"#"*v} {v}')

    html.write(str(resource_string('resources', 'abstracts.prolog'), 'utf-8'))
    csv_writer.writeheader()
    for cluster_idx in [x for x in clusters if x >= 0]:
        html.write(f'<h2>Cluster {cluster_idx}</h2>\n')
        for idx in clusters[cluster_idx]:
            entry = [x for x in ids if x['id'] == inc[idx]['id']][0]
            write_document(html, csv_writer, entry);
    html.write(str(resource_string('resources', 'abstracts.epilog'), 'utf-8'))


def setup_parse(argv):
    with open(argv[0], 'r', newline='') as ids_file, \
         open(argv[2], 'r', newline='') as abstracts_file, \
         open(argv[3], 'w', newline='') as html_file, \
         open(argv[4], 'w', newline='') as csv_file:
        ids = [x for x in csv.DictReader(ids_file)]
        tags_dict = tags.TagsDict(argv[1])
        tagged = [x for x in csv.DictReader(abstracts_file)]
        eps = 0.05 if len(argv) < 6 else float(argv[5])
        setup(ids, tags_dict, tagged, html_file, csv_file, eps=eps)


def exclude_extended(rows, extended_rows, csv_file):
    # remove duplicate ids
    rem = {i for i in range(len(rows)) if \
           len([j for j in range(i+1, len(rows)) if rows[j]['id'] == rows[i]['id']]) > 0}
    rows = [rows[i] for i in range(len(rows)) if i not in rem]
    extended_count = 0
    for e in extended_rows:
        cand = [x for x in rows if x['id'] == e['id']]
        assert len(cand) <= 1 # rows has no duplicate ids
        if len(e['decision'].strip()) > 0:
            extended_count = extended_count + 1
            cand[0]['tags'] = cand[0]['tags'] + ' ' + e['decision'].strip()
    csv_writer = csv.DictWriter(csv_file, fieldnames=['id', 'tags'])
    csv_writer.writeheader()
    for e in rows:
        csv_writer.writerow(e)
    print(f'Removed {len(rem)} duplicate IDs and {extended_count} entries with extensions')
    

def exclude_extended_parse(argv):
    with open(argv[0], 'r', newline='') as abstracts_file, \
         open(argv[1], 'r', newline='') as extended_file, \
         open(argv[2], 'w', newline='') as csv_file:
        rows = [x for x in csv.DictReader(abstracts_file)]
        extended_rows = [x for x in csv.DictReader(extended_file)]
        exclude_extended(rows, extended_rows, csv_file)
        
