import csv

class TagsDict(dict):
    def __init__(self, path):
        with open(path, 'r', newline='') as tags_file:
            tag_entries = [x for x in csv.DictReader(tags_file, skipinitialspace=True)]
            for tp in ['inc', 'exc', 'inf']:
                self[tp] = {x['tag'].lower() for x in tag_entries if x['type'] == tp}

    def is_inc_loose(self, entry):
        inc = [x for x in entry['tags'].lower().split() if x in self['inc']]
        exc = [x for x in entry['tags'].lower().split() if x in self['exc']]
        return len(inc) > 0 and len(exc) == 0
