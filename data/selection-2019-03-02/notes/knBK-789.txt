This paper proposes DISMED, a federated query mediator to SPARQL endpoints. The system was developed in the context of biomedical information, but is not tied to it.

There is few information about source selection, but it is performed from an index of sources maintained by DISMED. Query decomposition is done by groups of triple patterns that share the same subject. This kind of  assumes sharding of subjects: it is not a strong assumption and violating the sharding assumption may not bring any incompleteness in many cases. The engine works by first creating a SPARQL 1.1 federated query (with the SERVICE keyword) and then it executes such query using a specific engine which is part of the proposal. There are two alternative join algorithms: nested loop join, from Jena's ARQ and a bind join variant (it pushes filters deeper in the call chain).

Experiments use an ad-hoc federation scenario using the following datasets: BEMM, Diseaseome, DrugBank and Uniprot, using queries designed by the authors. SemWIQ and DARQ are used as baseline.

Source code: http://dismed.sourceforge.net

