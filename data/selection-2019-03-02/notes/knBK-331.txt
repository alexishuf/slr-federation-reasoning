## knBK-562
Proposes HiBISCuS, a join-aware source selection approach. Relevant sources are selected from an index and candidate sources are prunned if the algorithm determines from the joins that a source's results for a triple pattern would fail a join and should therefore not be fetched.

The index stores for each data souce, capabilities in the form ⟨ p, SA(p), OA(p) ⟩, where p is a predicate, SA(p) is a set of URI authorities of nodes used as subjects of p and OA(p) is the set of URI authorities of nodes used as objects of p.

The prunning criteria is the following: if two triple patterns participate in a join, sharing a variable in object or subject position (the variable may appear in a different position in one of the triple patterns), then only the intersection of OA or SA for the variable in both triples will be relevant for both triples. Any authority in one the two triples outside of the intersection will be eliminated during the join. If all authorithies of a source are eliminated in such manner, there is no need to query that source.

Experiments use the FedBench dataset. HiBISCuS was implemented on DARQ, SPLENDID and FedX. An ANAPSID extension (knBK-796) is used only as a baseline (without HiBISCuS). Machines used had 8GB of RAM. 

## knBK-331
Extension to HiBISCuS, named UPSP to use "unique predicates" instead of URI authorities to prune sources. An unique predicate is a predicate that appears as a predicate in only a single datasource and fits into one of the following four types.
- subject-subject: No subjects of the predicate appear as subjects in other datasets
- subject-object : No subjects of the predicate appear as objects  in other datasets
-  object-subject: No objects  of the predicate appear as subjects in other datasets
-  object-subject: No objects  of the predicate appear as objects  in other datasets
These properties of the definitions are used to prune data sources. Suppose a star shaped query ?s p1 ?v1; p2 ?v2. If p1 is subject-subject unique, the single dataset relevant for ?s p1 ?v1 is the single dataset relevant for the whole query as it is known that all subjects that satisfy the first triple pattern are in this dataset.

Experiments used FedBench on EC2 with 4-core Xenon machines with 15GB RAM. Systems compared were FedX and SPLENDID, each with three variants: original implementation, with HiBISCuS, and with HiHISCuS and UPSP (Unique Predicate Source Pruning).
