Presents GUN, a federated query mediator with mapping support at query-time that processes federated queries by dynamically building a temporary graph where the query is executed locally. This graph building strategy has the advantage that in scenarios with many LAV mappings (or sources deemed relevant), less query-rewritings will be submitted to the remote sources. 

The paper does not make it entirely clear if rewritings are also subqueries or if they are full rewritings (simple application of mappings).

Experiments do not compare against other federated query mediators. The benchmark used was a BSBM-generated (Berlin SPARQL Benchmark) 5M triples distributed in 10 views.
