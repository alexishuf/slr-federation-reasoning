This paper proposes a SPARQL query engine that decomposes queries into queries to multiple data sources with distinct data models (e.g., graph, relational, CSV and RDF). 

The datasources are listed in an  index kept in-memory. The index maintains which predicates are present in each data source, as well as mappings from the native data-model to RDF. Source selection is done with respect to the predicates present in each source. When a triple pattern has an unbound predicate but bound predicate or object, ASK queries are used to select sources. 

During query decomposition, a BGP may be broken into fragments due to unsafe predicates for a source. If a source contain some but not all predicates involved, sending triple patterns with the missing (i.e., unsafe) predicate will zero-out the result set.

Query optimization consists in reducing the number of joins using a Predicate-based Joing Group technique. Joins are ordered according to the number of free variables. Joins are implemented using nested joins.

Evaluations compare PolyStore to FedX. PolyStore has faster source selection, but slower query performance, overall. The datasets used are related to life sciences (and the BIOOPENER parent project). Queries are not shown in the paper, but there is a [link](https://drive.google.com/file/d/0B9vOyQJvl3qnYTl1ZmN5dUk0NHc/view) on the PolyWeb [github page](https://github.com/yasarkhangithub/PolyWeb).

The content in knBK-37 is a subset of that in knBK-3
