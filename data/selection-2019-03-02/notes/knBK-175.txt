[knBK-172]
PeNeLoop is a join algorithm developed for use in federated queries over endpoints with partial data replication. The basis join algorithm is nested loop join. The paper does not propose a query decomposition scheme, but experiments evaluate PeNeLoop implemented as within FedX together with Fedra. The dataset used in experiments is WatDiv with 10⁵ triples.

[knBK-175]
Implements PeNeLoop into the TPF client and provides more experiments

Source:
https://github.com/Callidon/peneloop-fedx
