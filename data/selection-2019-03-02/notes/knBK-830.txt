Proposes a method for summarizing multiple RDF datasets. The paper then briefly proposes source selection and join elimination (i.e., query decomposition).

The summaries are built first locally at each source and then merged in a central node. Locally, all nodes that share the same class and the same hash value are summarized to a single node. In the merged graph all graphs are united, unifying nodes that are likely to be correferent. The merged summary maintains information about the sources of each node, in order to make source selection possible.

The Source selection method proposed consists in querying each triple pattern against the summary graph and obtaining the source of the summary nodes selected.

the paper proposes join-elimination strategies, which can be seen as query decomposition strategies which merge triple patterns in order to send larger queries to sources instead of sending each triple pattern independently. The first proposal is highly similar to the exclusive groups from FedX, changing only in its relation to the summary. 

The second strategy is based on the heuriostic that when an ontology/vocabulary is reused by multiple datasets, triples using predicates from said ontologies will link locally-described subjects to locally described objects, therefore forgoing the need of inter-dataset joins.

Experiments compare FedX and Alibaba (a predecessor of FedX without published paper) on the Fedbench benchmark and on the UOBM benchmark (a improvement of LUBM). The query mediator ran on a notebook with 3GB RAM and Sesame SPARQL endpoints ran on 25 GB servers.
